import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SourcesService } from '../../services/sources.service'
import { SelectedSource } from 'src/app/models';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-source-feeds-selector-modal',
  templateUrl: './source-feeds-selector-modal.page.html',
  styleUrls: ['./source-feeds-selector-modal.page.scss'],
})
export class SourceFeedsSelectorModalPage implements OnInit, OnDestroy {

  @Input() allFeeds: SelectedSource[];
  @Input() userFeedsId: string[];
  selectedFeedsID: string[] = [];
  feeds: SelectedSource[];

  constructor(
    public modalController: ModalController,
    public sourcesService: SourcesService
    ) {
   }

  ngOnInit() {
    this.feeds = [...this.allFeeds];
    this.selectedFeedsID = this.userFeedsId ? [...this.userFeedsId] : [];
    if (this.userFeedsId && this.userFeedsId.length) {
      this.feeds.map( (feed) => {
        feed.selected = this.selectedFeedsID.includes(feed.id);
      });
    } else {
      this.feeds.map( (feed) => {
        feed.selected = false;
      });
    }
  }

  ngOnDestroy() {}

  toggleFeed(selected: boolean, id: string) {
    if (selected) {
      if (!this.selectedFeedsID.includes(id)) {
        this.selectedFeedsID.push(id);
      }
    } else {
      const index = this.selectedFeedsID.indexOf(id);
      if (index > -1) {
        this.selectedFeedsID.splice(index, 1);
      }
    }
  }


  save() {
    let selectedSources = this.selectedFeedsID.length ? this.allFeeds.filter(feed => this.selectedFeedsID.includes(feed.id)) : [];
    this.sourcesService.postSelectedSources(this.selectedFeedsID)
      .pipe(untilDestroyed(this))
      .subscribe(
        () => {
          this.modalController.dismiss({
            selectedSources: selectedSources,
            selectedFeedsID: this.selectedFeedsID
          })
        });
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
