import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SourceFeedsSelectorModalPageRoutingModule } from './source-feeds-selector-modal-routing.module';

import { SourceFeedsSelectorModalPage } from './source-feeds-selector-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SourceFeedsSelectorModalPageRoutingModule
  ],
  declarations: [SourceFeedsSelectorModalPage]
})
export class SourceFeedsSelectorModalPageModule {}
