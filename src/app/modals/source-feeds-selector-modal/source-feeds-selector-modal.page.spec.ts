import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SourceFeedsSelectorModalPage } from './source-feeds-selector-modal.page';

describe('SourceFeedsSelectorModalPage', () => {
  let component: SourceFeedsSelectorModalPage;
  let fixture: ComponentFixture<SourceFeedsSelectorModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourceFeedsSelectorModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SourceFeedsSelectorModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
