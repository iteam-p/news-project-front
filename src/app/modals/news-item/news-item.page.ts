import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-news-item',
  templateUrl: './news-item.page.html',
  styleUrls: ['./news-item.page.scss'],
})
export class NewsItemPage implements OnInit {
  @Input() link: string;

  constructor(private modalController: ModalController,
              private sanitizer: DomSanitizer
              ) { }

  ngOnInit() {
  }

  get linkURL() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(this.link);
  }

  dismiss(): void {
    this.modalController.dismiss();
  }
}
