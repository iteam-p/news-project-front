import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup;
  username: FormControl;
  password: FormControl;
  repeatedPassword: FormControl;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.username = formBuilder.control('', [
      Validators.required,
      Validators.pattern("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
    ]);
    this.password = formBuilder.control('', [Validators.required]);
    this.repeatedPassword = formBuilder.control('', [Validators.required]);
  }

  ngOnInit() {
    this.registerForm =  this.formBuilder.group({
      username: this.username,
      password: this.password,
      repeatedPassword: this.repeatedPassword
    });
  }

  register() {
    
  }

}
