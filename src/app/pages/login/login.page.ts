import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;
  username: FormControl;
  password: FormControl;

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.username = formBuilder.control('', [
      Validators.required,
      Validators.pattern("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
    ]);
    this.password = formBuilder.control('', [Validators.required]);
  }

  ngOnInit() {
    this.loginForm =  this.formBuilder.group({
      username: this.username,
      password: this.password
    });
  }

  login() {
    
  }

}
