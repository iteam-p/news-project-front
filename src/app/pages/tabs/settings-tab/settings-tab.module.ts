import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { TranslateModule } from '@ngx-translate/core';

import { SettingsTabPageRoutingModule } from './settings-tab-routing.module';

import { SettingsTabPage } from './settings-tab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SettingsTabPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [SettingsTabPage],
  providers: [SocialSharing]
})
export class SettingsTabPageModule {}
