import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { TranslateService } from '@ngx-translate/core';

import { ThemeService } from 'src/app/services';

export interface Language {
  value: string;
  label: string;
}

@Component({
  selector: 'app-settings-tab',
  templateUrl: './settings-tab.page.html',
  styleUrls: ['./settings-tab.page.scss'],
})

export class SettingsTabPage implements OnInit {
  currentLanguage: string;
  themeIsDark = false;
  languages: Language[] = [
    {
      value: 'en',
      label: 'En'
    },
    {
      value: 'ru',
      label: 'Ru'
    },
    {
      value: 'ukr',
      label: 'Ukr'
    }
  ];

  constructor(
    private themeService: ThemeService,
    private translateService: TranslateService,
    private socialSharing: SocialSharing
  ) { }

  ngOnInit() {
    this.themeIsDark = this.themeService.isDarkTheme();
  }

  toggleTheme(): void {
    if (this.themeService.isDarkTheme()) {
      this.themeService.setLightTheme();
    } else {
      this.themeService.setDarkTheme();
    }
    this.themeIsDark = this.themeService.isDarkTheme();
  }

  changeLang(language: string): void {
    this.translateService.use(language);
  }

  shareAppLink(): void {
    this.socialSharing.share('News UA', null, null, 'https://www.google.com.ua/')
      .then(success => {},
            error => {});
  }
}
