import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'my-feed-tab',
        loadChildren: () => import('./my-feed-tab/my-feed-tab.module').then(m => m.MyFeedTabPageModule)
      },
      {
        path: 'feeds-tab',
        loadChildren: () => import('./feeds-tab/feeds-tab.module').then(m => m.FeedsTabPageModule)
      },
      {
        path: 'bookmarks',
        loadChildren: () => import('./bookmarks-tab/bookmarks-tab.module').then(m => m.BookmarksTabPageModule)
      },
      {
        path: 'settings',
        loadChildren: () => import('./settings-tab/settings-tab.module').then(m => m.SettingsTabPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/my-feed-tab',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/main/tabs/my-feed-tab',
    pathMatch: 'full'
  },
  {
    path: 'bookmarks-tab',
    loadChildren: () => import('./bookmarks-tab/bookmarks-tab.module').then( m => m.BookmarksTabPageModule)
  },
  {
    path: 'settings-tab',
    loadChildren: () => import('./settings-tab/settings-tab.module').then( m => m.SettingsTabPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
