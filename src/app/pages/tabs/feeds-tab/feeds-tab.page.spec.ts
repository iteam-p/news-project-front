import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { FeedsTabPage } from './tab2.page';

describe('FeedsTabPage', () => {
  let component: FeedsTabPage;
  let fixture: ComponentFixture<FeedsTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FeedsTabPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FeedsTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
