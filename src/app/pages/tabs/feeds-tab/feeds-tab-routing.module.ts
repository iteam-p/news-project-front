import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FeedsTabPage } from './feeds-tab.page';

const routes: Routes = [
  {
    path: '',
    component: FeedsTabPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FeedsTabPageRoutingModule {}
