import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, LoadingController } from '@ionic/angular';

import { SourceFeedsSelectorModalPage } from '../../../modals/source-feeds-selector-modal/source-feeds-selector-modal.page';
import { SourcesService } from '../../../services/sources.service';
import { TabsService } from '../../../services/tabs.service';
import { Source, SelectedSource } from 'src/app/models';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'app-feeds-tab',
  templateUrl: 'feeds-tab.page.html',
  styleUrls: ['feeds-tab.page.scss']
})
export class FeedsTabPage implements OnInit, OnDestroy {
  userFeeds: Source[] = [];
  allFeeds: SelectedSource[];
  userFeedsId: string[];

  isLoading: boolean;

  constructor(
    public modalController: ModalController,
    public sourcesService: SourcesService,
    public loadingController: LoadingController,
    public tabsService: TabsService,
    ) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.getUserSourses();
    this.sourcesService.getAllSources()
      .pipe(untilDestroyed(this))
      .subscribe( (data: SelectedSource[]) => {
        this.allFeeds = data;
      });
  }

  ngOnDestroy() {}

  getUserSourses(): void {
    this.sourcesService.getUserSources()
      .pipe(untilDestroyed(this))
      .subscribe((data: Source[]) => {

        this.isLoading = false;

        if (data && data.length) {
          this.userFeeds = data;
          this.userFeedsId = this.userFeeds.map((userFeed) => userFeed.id);
        }
      });
  }

  async presentModal(): Promise<void> {
    const modal = await this.modalController.create({
      component: SourceFeedsSelectorModalPage,
      componentProps: {
        allFeeds: this.allFeeds,
        userFeedsId: this.userFeedsId
      }
    });

    modal.onDidDismiss().then((data) => {
      if (data && data.data) {
        this.userFeeds = data.data.selectedSources;
        this.userFeedsId = data.data.selectedFeedsID;

        this.tabsService.updateNews();
      }
    });
    return await modal.present();
  }

}
