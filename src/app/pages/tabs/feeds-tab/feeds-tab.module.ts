import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FeedsTabPage } from './feeds-tab.page';

import { FeedsTabPageRoutingModule } from './feeds-tab-routing.module';
import { SourceFeedsSelectorModalPageModule } from '../../../modals/source-feeds-selector-modal/source-feeds-selector-modal.module';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    FeedsTabPageRoutingModule,
    SourceFeedsSelectorModalPageModule,
    TranslateModule.forChild()
  ],
  declarations: [
    FeedsTabPage
  ]
})
export class FeedsTabPageModule {}
