import { Component, OnInit, OnDestroy, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';

import { NewsService } from '../../../services/news.service';
import { TabsService } from '../../../services/tabs.service';
import { NewsPiece } from 'src/app/models';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { LoadingController, IonItemSliding, IonContent, ModalController } from '@ionic/angular';
import { NewsItemPage } from 'src/app/modals/news-item/news-item.page';


@Component({
  selector: 'app-my-feed-tab',
  templateUrl: 'my-feed-tab.page.html',
  styleUrls: ['my-feed-tab.page.scss']
})
export class MyFeedTabPage implements OnInit, OnDestroy {

  @ViewChild(IonContent, { static: false }) content: IonContent;

  newsItems: NewsPiece[];
  pageCount: number;
  isLoading = false;
  updatedNews: boolean;

  constructor(
    private newsService: NewsService,
    private iab: InAppBrowser,
    public loadingController: LoadingController,
    public tabsService: TabsService,
    private modalController: ModalController
    ) {
  }

  ngOnInit() {
    this.isLoading = true;
    this.getInitialNews();
    this.tabsService.getNewsObservable()
      .pipe(untilDestroyed(this))
      .subscribe(() => {
        this.isLoading = true;
        this.updatedNews = true;
        this.getInitialNews();
      });
  }

  ngOnDestroy() {}

  ionViewDidEnter() {
    if (this.updatedNews) {
      this.content.scrollToTop(0);
      this.updatedNews = false;
    }
  }

  getInitialNews(): void {
    this.pageCount = 0;
    this.newsItems = [];
    this.newsService.getNews(this.pageCount)
      .pipe(untilDestroyed(this))
      .subscribe(data => {
        this.isLoading = false;
        this.newsItems = data;
      });
  }

  loadData(event): void {
      this.pageCount++;

      this.newsService.getNews(this.pageCount)
        .pipe(untilDestroyed(this))
        .subscribe( (data) => {
          event.target.complete();
          data.forEach(item => {
            if (this.newsItems.findIndex(newsItem => newsItem.id === item.id) < 0) {
              this.newsItems.push(item);
            }
          });
        });
  }

  async openItem(link: string): Promise<void> {
    const browser = this.iab.create(link, '_blank');
    browser.show();

    const modal = await this.modalController.create({
      component: NewsItemPage,
      componentProps: {
        link
      }
    });
    return await modal.present();
  }

  // async presentLoader() {
  //   this.isLoading = true;
  //   return await this.loadingController.create({
  //     spinner: 'bubbles',
  //     cssClass: 'custom-loader',
  //     backdropDismiss: true,
  //     showBackdrop: false
  //   }).then((loader) => {
  //     loader.present().then(() => {
  //       if (!this.isLoading) {
  //         loader.dismiss()
  //       }
  //     });
  //   });
  // }

  // async dismissLoader() {
  //   this.isLoading = false;
  //   return await this.loadingController.dismiss();
  // }

  doRefresh(event) {
    this.isLoading = true;
    this.getInitialNews();
    event.target.complete();
  }

  deleteNewsItem(newsItemID: string) {
    const index = this.newsItems.findIndex((el) => el.id === newsItemID);
    if (index > -1) {
      this.newsItems.splice(index, 1);
    }
    this.newsService.deleteNewsItem(newsItemID).subscribe();
    this.content.scrollToTop(0);
  }

  addToFavourites(slidingItem: IonItemSliding, newsItemID: string) {
    slidingItem.close();

    this.newsItems.forEach(item => {
      if (item.id === newsItemID) {
        item.bookmark = true;
      }
    });

    this.newsService.addToFavourites(newsItemID).subscribe();
  }

}
