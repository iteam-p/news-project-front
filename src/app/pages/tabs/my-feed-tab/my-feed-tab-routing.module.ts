import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyFeedTabPage } from './my-feed-tab.page';

const routes: Routes = [
  {
    path: '',
    component: MyFeedTabPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyFeedTabPageRoutingModule {}
