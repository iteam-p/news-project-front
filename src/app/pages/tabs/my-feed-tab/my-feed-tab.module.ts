import { IonicModule, IonInfiniteScroll } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Device } from '@ionic-native/device/ngx';

import { MyFeedTabPage } from './my-feed-tab.page';

import { MyFeedTabPageRoutingModule } from './my-feed-tab-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { UtilsModule } from 'src/app/utils/utils.module';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    UtilsModule,
    HttpClientModule,
    MyFeedTabPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [MyFeedTabPage],
  providers: [
    Device,
    InAppBrowser
  ]
})
export class MyFeedTabPageModule {}
