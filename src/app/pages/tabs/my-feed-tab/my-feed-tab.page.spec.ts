import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyFeedTabPage } from './my-feed-tab.page';

describe('MyFeedTabPage', () => {
  let component: MyFeedTabPage;
  let fixture: ComponentFixture<MyFeedTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MyFeedTabPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyFeedTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
