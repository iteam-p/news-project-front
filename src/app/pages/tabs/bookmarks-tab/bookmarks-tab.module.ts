import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookmarksTabPageRoutingModule } from './bookmarks-tab-routing.module';

import { BookmarksTabPage } from './bookmarks-tab.page';
import { UtilsModule } from 'src/app/utils/utils.module';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UtilsModule,
    BookmarksTabPageRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [BookmarksTabPage],
  providers: [InAppBrowser]
})
export class BookmarksTabPageModule {}
