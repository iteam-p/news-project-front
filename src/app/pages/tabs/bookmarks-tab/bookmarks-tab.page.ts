import { Component, OnInit } from '@angular/core';
import { NewsService } from 'src/app/services';
import { LoadingController } from '@ionic/angular';
import { NewsPiece } from 'src/app/models';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'app-bookmarks-tab',
  templateUrl: './bookmarks-tab.page.html',
  styleUrls: ['./bookmarks-tab.page.scss'],
})
export class BookmarksTabPage implements OnInit {

  favouriteNews: NewsPiece[];
  pageCount: number;

  isLoading: boolean;

  constructor(
    private newsService: NewsService,
    private iab: InAppBrowser,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.getInitialFavourites();
  }

  ngOnDestroy() {}

  ionViewWillEnter() {
    this.getInitialFavourites();
  }

  getInitialFavourites(): void {
    this.pageCount = 0;
    this.newsService.getFavouriteNews(this.pageCount)
      .pipe(untilDestroyed(this))
      .subscribe(data => {
        this.isLoading = false;
        this.favouriteNews = data;
      });
  }

  loadData(event): void {
      this.pageCount++;

      this.newsService.getFavouriteNews(this.pageCount)
        .pipe(untilDestroyed(this))
        .subscribe( (data) => {  
          data.forEach(item => this.favouriteNews.push(item));
          event.target.complete();
        });
  }

  openItem(link: string): void {
    const browser = this.iab.create(link, '_blank');
    browser.show();
  }

  deleteFromFavourites(newsItemID: string) {
    const index = this.favouriteNews.findIndex((el) => el.id === newsItemID);
    if (index > -1) {
      this.favouriteNews.splice(index, 1);
    }
    this.newsService.deleteFromFavourites(newsItemID).subscribe();
  }
}
