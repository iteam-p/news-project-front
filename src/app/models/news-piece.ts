
export class NewsPiece {

    id: string;
    title: string;
    imgUrl: string;
    link: string;
    content: string;
    pubdate: Date | null;
    bookmark: boolean;

    constructor(data) {
        this.id = data.id || null;
        this.title = data.title || '';
        this.imgUrl = data.img_url || './../../../assets/images/news_placeholder.svg';
        this.link = data.link || '';
        this.content = data.content_snippet || '';
        this.pubdate = data.pub_date ? new Date(data.pub_date) : null;
        this.bookmark = data.bookmark || false;
    }
}
