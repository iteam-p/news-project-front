export class Source {

    id: string | null;
    name: string;
    url: string;
    imgUrl: string;

    constructor(data) {
        this.id = data.id || null;
        this.name = data.name || '';
        this.url = data.url || '';
        this.imgUrl = data.img_url || './../../../assets/images/news_placeholder.svg';
    }
}

export class SelectedSource extends Source {
    selected: boolean;
}