export interface Theme {
    name: string;
    properties: any;
  }

export const light: Theme = {
  name: 'light',
  properties: {
    '--background-primary': '#fff',
    '--background-secondary': '#fff',

    '--color-primary': '#000',
    '--color-secondary': '#777',

    '--color-button': 'rgb(102, 102, 102)',
    '--color-button-selected': '#004b77',

  }
};

export const dark: Theme = {
  name: 'dark',
  properties: {
      '--background-primary': '#444',
      '--background-secondary': '#222',

      '--color-primary': '#fff',
      '--color-secondary': '#999',

      '--color-button': '#fff',
      '--color-button-selected': '#3dc2ff'
  }
};
