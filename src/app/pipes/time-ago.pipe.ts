import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'timeAgo',
  pure: false
})
export class TimeAgoPipe implements PipeTransform {

  constructor(private translateService: TranslateService) {}

  transform(pubdate: Date): unknown {
    let result: string;
    // time since message was sent in seconds
    const delta = (new Date().getTime() - pubdate.getTime()) / 1000;

    // format string
    if (delta < 60)
    { // sent in last minute
        result = `${Math.floor(delta)} ${this.translateService.instant('seconds_ago')}`;
    }
    else if (delta < 3600)
    { // sent in last hour
        result = `${Math.floor(delta / 60)} ${this.translateService.instant('minutes_ago')}`;
    }
    else if (delta < 86400)
    { // sent on last day
        result = `${Math.floor(delta / 3600)} ${this.translateService.instant('hours_ago')}`;
    }
    else
    { // sent more than one day ago
      let month = pubdate.getMonth() < 10 ? `0${pubdate.getMonth()}` : `${pubdate.getMonth()}`;

        result = `${pubdate.getDate()}.${month}.${pubdate.getFullYear()}`;
    }
    return result;
  }


}
