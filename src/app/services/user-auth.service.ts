import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserAuthService {
  urlApi = environment.apiUrl;
  private _userId: string;

  get userId() {
    return this._userId;
  }

  set userId(id) {
     this._userId = id;
  }

  constructor(
    protected http: HttpClient,
  ) { }

  setUserId(id: any) {
    this.userId = id.toString();
    return this.http.get(`${this.urlApi}/users/user_id/${this.userId}`);
  }
}
