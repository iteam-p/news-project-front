import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Source, SelectedSource } from '../models';
import { UserAuthService } from './user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class SourcesService {
  urlApi = environment.apiUrl;
  userId: string;

  constructor(
    protected http: HttpClient,
    private userAuthService: UserAuthService
  ) {
    this.userId = this.userAuthService.userId;
  }

  getAllSources() {
    return this.http.get(`${this.urlApi}/sources/all`)
      .pipe(
        map((data: any[]) => {
          return data.map(item => new SelectedSource(item));
        }));
  }

  getUserSources() {
    return this.http.get(`${this.urlApi}/sources/${this.userId}`)
      .pipe(
        map((data: any[]) => {
          return data.map(item => new Source(item));
        }));
  }

  postSelectedSources(selectedSources: string[]) {
    return this.http.post(`${this.urlApi}/sources`,
          {
            userId: this.userId,
            selectedSourcesId: selectedSources
          });
  }
}
