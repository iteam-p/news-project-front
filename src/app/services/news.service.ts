import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NewsPiece } from '../models/news-piece';
import { environment } from 'src/environments/environment';
import { UserAuthService } from './user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class NewsService {
  urlApi = environment.apiUrl;
  userId: string;

  constructor(
    protected http: HttpClient,
    private userAuthService: UserAuthService
  ) {
    this.userId = this.userAuthService.userId;
  }

  getNews(page: number) {
    return this.http.get(`${this.urlApi}/news/${page}/${this.userId}`)
    .pipe(
      map((data: any[]) => {
        return data.map(item => new NewsPiece(item));
      }));
  }

  getFavouriteNews(page: number) {
    return this.http.get(`${this.urlApi}/news/favourites/${page}/${this.userId}`)
    .pipe(
      map((data: any[]) => {
        return data.map(item => new NewsPiece(item));
      }));
  }

  deleteNewsItem(newsItemID: string) {
    return this.http.post(`${this.urlApi}/news/delete/${this.userId}`, { newsItemID });
  }

  addToFavourites(newsItemID: string) {
    return this.http.post(`${this.urlApi}/news/favourites/${this.userId}`, { newsItemID });
  }

  deleteFromFavourites(newsItemID: string) {
    return this.http.post(`${this.urlApi}/news/favourites/delete/${this.userId}`, { newsItemID });
  }
}
