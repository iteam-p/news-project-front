import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TabsService {

  updateNewsSubject = new Subject<any>();

  updateNews() {
      this.updateNewsSubject.next();
  }

  getNewsObservable(): Subject<any> {
      return this.updateNewsSubject;
  }
}
