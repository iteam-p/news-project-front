import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { UserAuthService } from './services/user-auth.service';
import { Device } from '@ionic-native/device/ngx';
import { TranslateService } from '@ngx-translate/core';
import { ThemeService } from './services';
import { dark, light } from './models';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit{

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private userAuthService: UserAuthService,
    private device: Device,
    private translateService: TranslateService,
    private themeService: ThemeService
  ) {
    this.initializeApp();
    this.translateService.setDefaultLang('en');
  }

  ngOnInit(): void {
    this.translateService.use('ru');
    this.translateService.use('ukr');
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.userAuthService.setUserId(this.device.uuid).subscribe();
    });
    this.userAuthService.setUserId('hdgk9749gfj89999').pipe(take(1)).subscribe();
    this.themeService.setActiveTheme(light);
  }
}
